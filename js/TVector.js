export class TVector
{
    constructor(x = 0, y = 0)
    {
        this.x = x;
        this.y = y;
    }

	// TVector(x, y)
	// {
	// 	this.x = x;
	// 	this.y = y;
	// }
}

TVector.prototype.cloneFromPoint = function(p) {
    this.x = p.x;
    this.y = p.y;
    return this;
};

TVector.prototype.add = function(v) {
    this.x += v.x;
    this.y += v.y;
    return this;
};

TVector.prototype.sub = function(v) {
    this.x -= v.x;
    this.y -= v.y;
    return this;
};

TVector.prototype.invert = function(v) {
    this.x *= -1;
    this.y *= -1;
    return this;
};

TVector.prototype.multiplyScalar = function(s) {
    this.x *= s;
    this.y *= s;
    return this;
};

TVector.prototype.divideScalar = function(s) {
    if(s === 0) {
        this.x = 0;
        this.y = 0;
    } else {
        var invScalar = 1 / s;
        this.x *= invScalar;
        this.y *= invScalar;
    }
    return this;
};

TVector.prototype.dot = function(v) {
    return this.x * v.x + this.y * v.y;
};

TVector.prototype.length = function(v) {
    return Math.sqrt(this.x * this.x + this.y * this.y);
};

TVector.prototype.lengthSq = function() {
    return this.x * this.x + this.y * this.y;
};

TVector.prototype.normalize = function() {
    return this.divideScalar(this.length());
};

TVector.prototype.distanceTo = function(v) {
    return Math.sqrt(this.distanceToSq(v));
};

TVector.prototype.distanceToSq = function(v) {
    var dx = this.x - v.x, dy = this.y - v.y;
    return dx * dx + dy * dy;
};

TVector.prototype.set = function(x, y) {
    this.x = x;
    this.y = y;
    return this;
};

TVector.prototype.setX = function(x) {
    this.x = x;
    return this;
};

TVector.prototype.setY = function(y) {
    this.y = y;
    return this;
};

TVector.prototype.setLength = function(l) {
    var oldLength = this.length();
    if(oldLength !== 0 && l !== oldLength) {
        this.multiplyScalar(l / oldLength);
    }
    return this;
};

TVector.prototype.invert = function(v) {
    this.x *= -1;
    this.y *= -1;
    return this;
};

TVector.prototype.lerp = function(v, alpha) {
    this.x += (v.x - this.x) * alpha;
    this.y += (v.y - this.y) * alpha;
    return this;
};

TVector.prototype.rad = function() {
    return Math.atan2(this.x, this.y);
};

TVector.prototype.deg = function() {
    return this.rad() * 180 / Math.PI;
};

TVector.prototype.equals = function(v) {
    return this.x === v.x && this.y === v.y;
};

TVector.prototype.rotate = function(theta) {
    var xtemp = this.x;
    this.x = this.x * Math.cos(theta) - this.y * Math.sin(theta);
    this.y = xtemp * Math.sin(theta) + this.y * Math.cos(theta);
    return this;
};

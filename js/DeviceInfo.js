class DeviceInfo
{
	constructor()
	{
		this.GAME_COUNTRY	= this.GetCurrentCountry()
		this.DEVICE_MODEL	= this.GetStatsUrlInfo('device_model')
		this.DEVICE_INFO	= ''
	}

	DetectDevice(width, height)
	{
		console.log('User Agent: ' + navigator.userAgent)
		console.log('Device Model: ' + this.DEVICE_MODEL)

		var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false )
		if ((iOS) || (navigator.userAgent.indexOf('Mac OS X') != -1))
		{
			this.DEVICE_INFO = 'iOS'
		}
		else 
		if (navigator.userAgent.indexOf('Android') != -1)
		{
			this.DEVICE_INFO = 'Android'
		}
		else
		{
			this.DEVICE_INFO = 'unknown'
		}
	}

	GetStatsUrlInfo(mask)
	{
		let result = ''
		if (typeof strStatsUrl != 'undefined')
		{
			let ii = strStatsUrl.indexOf(mask + '=')
			if (ii != -1)
			{
				ii += mask.length + 1
				let iii = strStatsUrl.indexOf('&', ii)
				result 	= strStatsUrl.substring(ii, iii)
			}
		}

		return result
	}

	GetCurrentCountry()
	{
		return (window.ipCountry || 'US').toUpperCase()
	}

	GetLanguagePack()
	{
		let languagePack =
		{
			FR: PACK_TEXT_FR,
			DE: PACK_TEXT_DE,
			IT: PACK_TEXT_IT,
			ES: PACK_TEXT_ES,
			BR: PACK_TEXT_BR,
			RU: PACK_TEXT_RU,
			PL: PACK_TEXT_PL,
			TR: PACK_TEXT_TR,
			TH: PACK_TEXT_TH,
			VI: PACK_TEXT_VI,
			JP: PACK_TEXT_JP,
			KR: PACK_TEXT_KR,
			AR: PACK_TEXT_AR,
		}
		return languagePack[this.GAME_COUNTRY] || PACK_TEXT_EN
	}
}
module.exports = new DeviceInfo()

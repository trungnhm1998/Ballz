import { Tile } from '../game/Tile'
import { TVector } from './../TVector.js';

class GameManager extends PIXI.Container
{
	constructor()
	{
		super();
		this.m_HighestRowScore = 0;
		this.m_TilesPosX = [];
		this.m_TilesPosY = [];
		this.m_TilePosIndex = [];
		this.m_Tiles = [];
		this.m_FirstRowIndex = [];
		this.m_HasGenerated;
		this.m_HasShiftedDown;
		this.m_IsLost;

		// if touch will hold the state of touch/click/pointer
		this.m_TouchDown = false;
		this.m_TouchUpPoint = new TVector();
	}

	Init()
	{
		this.m_IsLost = false;
		this.m_HasShiftedDown = false;
		this.m_HasGenerated = true;

	
		this.addBackgroundToStage();
		this.addGroundBackgroundAndHUDToStage();// serious the function name...

		BallManager.Init();
		
		this.initTilesPos();
		this.generateNewRow();

	}

	initTilesPos()
	{
		//console.log("initTilesPos() called");
		for (var i = 0; i < GameDefine.gGameRow; i++) {
			this.m_TilePosIndex.push(i * (GameDefine.gTileSize + GameDefine.gPaddingFromEachTile) + GameDefine.gPaddingFromOffsetX + GameDefine.gTileSize/2);
			this.m_TilesPosX.push(
				i * (GameDefine.gTileSize + GameDefine.gPaddingFromEachTile) + GameDefine.gPaddingFromOffsetX + GameDefine.gTileSize/2);
			this.m_TilesPosY.push(
				i * (GameDefine.gTileSize + GameDefine.gPaddingFromEachTile) + GameDefine.gPaddingFromOffsetY + GameDefine.gTileSize/2);
		}
	}

	Update(deltaTime)
	{
		for (var i = this.m_Tiles.length - 1; i >= 0; i--) {
			this.m_Tiles[i].Update(deltaTime);
		}

		if(!this.m_HasGenerated)//although it will call every tick but nothing will executed
		{
			this.generateNewRow();
		}

	}

	TouchHandler(event)
	{
		if(event.type == 'pointerdown')
		{
			if(event.data.global.y > GameDefine.gHUDHeight &&
				event.data.global.y < GameDefine.gBackgroundHeight - (GameDefine.gGroundHeight + GameDefine.gBallRadius))
			{
				this.m_TouchDown = true;
				BallManager.m_ToggleTouch = true;

				BallManager.m_TouchDownVector.x = event.data.global.x;
				BallManager.m_TouchDownVector.y = event.data.global.y;

				//reset m_TouchReleasedVector
				BallManager.m_TouchReleasedVector = new TVector();

			}
			else
			{
				BallManager.m_ToggleTouch = false;
				BallManager.m_TouchDownVector = new TVector();
				BallManager.m_TouchReleasedVector = new TVector();
			}

		}
		else if(event.type == 'pointerup')
		{
			if(event.data.global.y > GameDefine.gHUDHeight && 
				event.data.global.y < GameDefine.gBackgroundHeight - (GameDefine.gGroundHeight + GameDefine.gBallRadius))
			{
				this.m_TouchDown = false;

				BallManager.m_ToggleTouch = false;

				BallManager.m_TouchReleasedVector.x = event.data.global.x;
				BallManager.m_TouchReleasedVector.y = event.data.global.y;
			}
			else
			{
				BallManager.m_ToggleTouch = true;
				BallManager.m_TouchReleasedVector = new TVector();
			}
			// console.log(event.type);
		}

		if(this.m_TouchDown && event.type == 'pointermove')
		{
			if(event.data.global.y > GameDefine.gHUDHeight && 
				event.data.global.y < GameDefine.gBackgroundHeight - (GameDefine.gGroundHeight + GameDefine.gBallRadius))
			{
				if(!BallManager.m_ToggleTouch)
				{
					BallManager.m_ToggleTouch = true;
					BallManager.setBallOfLineVisible(false);
				}
				BallManager.m_TouchDownVector.x = event.data.global.x;
				BallManager.m_TouchDownVector.y = event.data.global.y;
			}
			else
			{
				BallManager.setBallOfLineVisible(false);
				BallManager.m_ToggleTouch = false;
				BallManager.m_TouchDownVector = new TVector();
				BallManager.m_TouchReleasedVector = new TVector();
			}
		}
	}

	KeyHandler(event)
	{
		if(Input.IsKeyUp(event))
		{
			console.log(event.code);
			switch (event.code)
			{
				case "Space":
				{
					console.log("out");
					this.shiftRowDown();
					break;
				}
				case "KeyG":
				{
					if(!this.m_IsLost)
						this.generateNewRow();
					break;
				}
				case "KeyR":
				{
					this.restartGame();
					break;
				}
			}
		}
	}

	// will shift all the tile down
	shiftRowDown()
	{
		// console.log("shiftRowDown called");
		for (var i = this.m_Tiles.length - 1; i >= 0; i--) {
			this.m_Tiles[i].moveTo(new PIXI.Point(this.m_TilesPosX[this.m_Tiles[i].m_IndexOnBoard.x], 
												this.m_TilesPosY[this.m_Tiles[i].m_IndexOnBoard.y + 1]),
												GameDefine.gShiftDownDelayFrame);
			this.m_Tiles[i].m_IndexOnBoard.y++;
		}
	}

	tweenShiftDownIsPlaying()
	{
		for (var i = this.m_Tiles.length - 1; i >= 0; i--) 
		{
			//console.log("this.m_Tiles[i].tweenIsPlaying(): "+ this.m_Tiles[i]);
			// console.log("tweenIsPlaying at: " + i);
			if(this.m_Tiles[i].tweenIsPlaying())
			{
				// console.log("tweenIsPlaying at: " + i);
				return true;
			}
		}
		return false;
	}

	// will generate new row above with higher tile score
	generateNewRow()
	{
		var FirstRowEmpty = false
		if(this.m_FirstRowIndex.length == 0)
			FirstRowEmpty = true;

		// clear the row index
		// only clear the last row if the game already generated the row can ready to shift down for new one
		if(this.m_HasGenerated || !FirstRowEmpty)
		{
			for (var index = 0; index < GameDefine.gGameColumn; index++) {
				this.m_FirstRowIndex[index] = false;
			}
		}

		// the first row must be not empty
		// and has generated the last time
		// will the we can shift down and start generates new row
		// console.log(FirstRowEmpty+"-"+this.m_HasGenerated);
		if(!FirstRowEmpty && this.m_HasGenerated)
		{
			// we can shift down here because has generated the row before
			// while shifting down we cannot gen new row so this.m_HasGenerated = false
			this.m_HasShiftedDown = true;
			this.m_HasGenerated = false;
			this.shiftRowDown();//Shift all tiles down before generated new row
		}
		
		// check if the tween is all done
		// then we can started to gererate new row
		if(!this.tweenShiftDownIsPlaying() || FirstRowEmpty)
		{
			this.m_HighestRowScore++;//Hit to kill go up
			let NumberOfTileWillBeRandom = Math.floor((Math.random() * (GameDefine.gGameColumn)) + 1);//Number of Tile have on the new row
			//console.log("NumberOfTileWillBeRandom: "+NumberOfTileWillBeRandom)
			let i = NumberOfTileWillBeRandom;
			let Column;
			while(i > 0)//random column position on the first row
			{
				Column = Math.floor(Math.random() * GameDefine.gGameColumn);
				if(!this.m_FirstRowIndex[Column])
				{
					this.m_FirstRowIndex[Column] = true;
					let TempTile = new Tile(new PIXI.Point(Column, 0));
					TempTile.Init(this.m_HighestRowScore);
					this.addTileToStage(TempTile);
					TempTile.m_Sprite.position.set(this.m_TilesPosX[Column], this.m_TilesPosY[0]);

					i--;
				}
			}
			// reset flag just done gernated so this.m_HasGerated = true
			// because just generated that's meant the we can shift down again so this.m_HasShiftedDown = true
			this.m_HasShiftedDown = false;
			this.m_HasGenerated = true;
		}		
		if(!this.m_IsLost)
		{
		// console.log("BEGIN");
			for (var i = 0; i < GameDefine.gGameColumn; i++) 
			{
				if(this.m_Tiles[i] != undefined)
				{
					// console.log("index: " + i + " - "+"rowIndex: "+this.m_Tiles[i].m_IndexOnBoard.y);
					if(this.m_Tiles[i] != undefined && this.m_Tiles[i].m_IndexOnBoard.y == GameDefine.gGameRow-1)
					{
						// console.log("YOU LOST");
						this.m_IsLost = true;
						break;
					}
				}
			}
		// console.log("END");
		}
	}

	restartGame()
	{
		for (var i = this.m_Tiles.length -1 ; i >= 0 ; i--) {
			this.removeTileToState(this.m_Tiles[i]);
			this.m_Tiles.pop();
		}
		// console.log("this.m_Tiles.length: "+this.m_Tiles.length);
		this.m_HighestRowScore = 0;
		this.m_IsLost = false;
		this.m_HasShiftedDown = false;
		this.m_HasGenerated = true;
		this.m_FirstRowIndex = [];
		this.generateNewRow();

	}

	removeTileToState(tile)
	{
		this.removeChild(tile.m_Sprite);
		this.removeChild(tile.m_Text);
	}

	addTileToStage(tile)
	{
		this.m_Tiles.push(tile);
		this.addChild(tile.m_Sprite);
		this.addChild(tile.m_Text);
	}

	addBackgroundToStage()
	{
		// draw background
		let graphics = new PIXI.Graphics();
		graphics.beginFill(GameDefine.gBackgroundColor);
		graphics.lineStyle(0, 0, 1);// no line

		graphics.drawRect(0, 0, GameDefine.gBackgroundWidth, GameDefine.gBackgroundHeight);

		graphics.endFill();

		this.addChild(graphics);
	}

	addGroundBackgroundAndHUDToStage()// what the...
	{
		// draw background
		let graphics = new PIXI.Graphics();
		graphics.beginFill(GameDefine.gMiscColor);
		graphics.lineStyle(0, 0, 1);// no line

		graphics.drawRect(0, 0, GameDefine.gBackgroundWidth, GameDefine.gHUDHeight);
		graphics.drawRect(0, GameDefine.gBackgroundHeight - GameDefine.gGroundHeight, 
						GameDefine.gBackgroundWidth, GameDefine.gGroundHeight);

		graphics.endFill();

		this.addChild(graphics);
	}

}
module.exports = new GameManager();
import { TVector } from './../TVector.js';

class BallManager
{
	constructor()
	{
		this.m_BallSprite;
		this.m_ToggleTouch = false;
		this.m_ShootDirectionSprite;
		this.m_Direction = new TVector();
		this.m_TouchDownVector = new TVector();// -1 is just a dumb position
		this.m_TouchReleasedVector = new TVector();

		this.m_BallTouchGround = true;
		
		this.m_LineOfBalls = [];
		this.m_Balls = [];

		this.m_HasCalculatedDirection = false;


		this.NUMBER_OF_BALL_FOR_SHOOTING_LINE = 15;
		this.DIRECTION_ROUND = 10;
	}

	Init()
	{
		this.m_Direction = new TVector(0, 0);

		let graphics = new PIXI.Graphics();
		graphics.lineStyle(0);
		graphics.beginFill(GameDefine.gBallColor, 1);
		graphics.drawCircle(0, 0, 
							GameDefine.gBallRadius);
		graphics.endFill();

		let texture = graphics.generateCanvasTexture();
		this.m_BallSprite = new PIXI.Sprite(texture);
		this.m_BallSprite.anchor.set(0.5);
		this.m_BallSprite.position.set(GameDefine.gBackgroundWidth/2, 
									GameDefine.gBackgroundHeight - GameDefine.gGroundHeight);


		for (var i = 0; i < this.NUMBER_OF_BALL_FOR_SHOOTING_LINE; i++) {
			graphics = new PIXI.Graphics();
			graphics.lineStyle(0);
			graphics.beginFill(GameDefine.gBallColor, 0.5);
			graphics.drawCircle(0, 0, 
								GameDefine.gBallRadius/2);
			graphics.endFill();
			texture = graphics.generateCanvasTexture();

			let m_ShootDirectionSprite = new PIXI.Sprite(texture);
			m_ShootDirectionSprite.anchor.set(0.5);
			m_ShootDirectionSprite.visible = false;
			
			GameManager.addChild(m_ShootDirectionSprite);
			this.m_LineOfBalls.push(m_ShootDirectionSprite);
			
		}

		GameManager.addChild(this.m_BallSprite);

	}

	Update(deltaTime)
	{

		this.checkBounder();

		if(this.m_ToggleTouch && this.m_BallTouchGround)// while the pointer is down calculated the direction to visualize where the ball will shoot
		{
			this.setBallOfLineVisible(this.m_ToggleTouch);

			let BallPos = new TVector();
			BallPos.cloneFromPoint(this.m_BallSprite.position);

			this.drawShootingLine(BallPos, this.m_TouchDownVector);

			this.m_HasCalculatedDirection = false;
		}
		else if(!this.m_ToggleTouch && this.m_TouchReleasedVector.length() != 0)// when pointer is up re calculated the direction and then shoot the ball
		{
			this.m_BallTouchGround = false;
			this.setBallOfLineVisible(this.m_ToggleTouch);
			//console.log(this.m_TouchReleasedVector);
			//console.log(BallPos);
			if(!this.m_HasCalculatedDirection)
			{
				let BallPos = new TVector();
				BallPos.cloneFromPoint(this.m_BallSprite.position);
				this.m_Direction = this.calculatedVector(BallPos, this.m_TouchReleasedVector);
				this.m_Direction.normalize();//normalize to get direction only
			}


			this.m_BallSprite.x += (this.m_Direction.x * this.DIRECTION_ROUND);
			this.m_BallSprite.y += (this.m_Direction.y * this.DIRECTION_ROUND);

		}
	}

	checkBounder()
	{
		if(this.m_BallSprite.x >= GameDefine.gBackgroundWidth)
			this.m_Direction.x *= -1;
		else if(this.m_BallSprite.x <= 0)
			this.m_Direction.x *= -1;

		if(this.m_BallSprite.y <= GameDefine.gHUDHeight)
			this.m_Direction.y *= -1;
		else if(this.m_BallSprite.y >= GameDefine.gBackgroundHeight - GameDefine.gGroundHeight)
		{
			if(this.m_BallSprite.x <= GameDefine.gBallRadius)
			{
				this.m_BallSprite.x = GameDefine.gBallRadius;
			}
			else if(this.m_BallSprite.x >= GameDefine.gBackgroundWidth - GameDefine.gBallRadius)
			{
				this.m_BallSprite.x = GameDefine.gBackgroundWidth - GameDefine.gBallRadius;
			}
			this.m_BallSprite.y = GameDefine.gBackgroundHeight - GameDefine.gGroundHeight;

			this.m_BallTouchGround = true;
			this.m_Direction.x = 0;
			this.m_Direction.y = 0;
		}
	}

	// given 2 point calculate the vector from that 2 points
	calculatedVector(beginPos, endPos)
	{
		this.m_HasCalculatedDirection = true;
		let DirectionalVector = new TVector(0, 0);
		DirectionalVector.x = endPos.x - beginPos.x;
		DirectionalVector.y = endPos.y - beginPos.y;

		return DirectionalVector;
	}

	drawShootingLine(ballPos, touchDownPos)
	{
		let WillShootDirection = new TVector();
		WillShootDirection = this.calculatedVector(ballPos , touchDownPos);
		let FromBallToTouchLength = WillShootDirection.length();
		let FromBallToNextBallOfLine = FromBallToTouchLength / this.NUMBER_OF_BALL_FOR_SHOOTING_LINE;
		WillShootDirection = WillShootDirection.normalize();

		WillShootDirection.multiplyScalar(FromBallToNextBallOfLine);

		let FinalVector = ballPos;
		FinalVector.add(WillShootDirection);

		// position.set(FinalVector.x, FinalVector.y);

		for (var i = 0; i < this.NUMBER_OF_BALL_FOR_SHOOTING_LINE; i++) 
		{
			this.m_LineOfBalls[i].position.set(FinalVector.x, FinalVector.y);
			FinalVector.add(WillShootDirection);
		}

	}

	setBallOfLineVisible(flag)
	{
		for (var i = 0; i < this.NUMBER_OF_BALL_FOR_SHOOTING_LINE; i++) 
		{
			this.m_LineOfBalls[i].visible = flag;	
		}
	}
}
module.exports = new BallManager();
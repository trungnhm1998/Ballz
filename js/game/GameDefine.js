class GameDefine
{
	constructor()
	{
		this.gGameRow		= 8;
		this.gGameColumn 	= 7;
		this.gTileSize		= 50;

		this.gPaddingFromOffsetX = 20;
		this.gPaddingFromOffsetY = 150;
		this.gPaddingFromEachTile = 10;

		this.gBallRadius = 12;

		this.gBackgroundColor = 0x202020;
		this.gBackgroundWidth = this.gGameColumn * this.gTileSize + ((this.gGameColumn + 1) * this.gPaddingFromEachTile) + this.gPaddingFromOffsetX;
		this.gBackgroundHeight = this.gGameRow * this.gTileSize + ((this.gGameRow + 1) * this.gPaddingFromEachTile) + (this.gPaddingFromOffsetY *2 );

		this.gHUDHeight	= 100;
		this.gGroundHeight = 150;

		this.gMiscColor = 0x262626;
		this.gTextColor = 0xFBFBFB;
		this.gBallColor = 0XFFFFFF;
		this.gPauseUIColor = 0x454545;
		this.gTileColor = [0xFF9B46];

		this.gShiftDownDelayFrame = 20;
	}
}
module.exports = new GameDefine();

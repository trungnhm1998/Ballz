export class Tile
{
	constructor(index)
	{
		this.m_IndexOnBoard = new PIXI.Point(index.x, index.y);
		this.m_HitToKill;
		this.m_Sprite;
		this.m_Text;
		this.m_SlideTween;
	}

	Init(number = 0)
	{
		//console.log("Init");
		let Graphics = new PIXI.Graphics();

		this.m_Text = new PIXI.Text(number);
		this.m_Text.position.set(100, 100);
		this.m_Text.anchor.set(0.5);

		Graphics.beginFill(GameDefine.gTileColor[0], 1);
		Graphics.drawRect(0, 0, GameDefine.gTileSize, GameDefine.gTileSize);
		Graphics.endFill();

		let Texture = Graphics.generateCanvasTexture();
		this.m_Sprite = new PIXI.Sprite(Texture);
		this.m_Text.position.set(this.m_Sprite.position.x, this.m_Sprite.position.y);
		this.m_Sprite.anchor.set(0.5, 0.5);

	}

	Update(deltaTime)
	{
		this.m_Text.position.set(this.m_Sprite.position.x, this.m_Sprite.position.y);
	}

	tweenIsPlaying()
	{
		// console.log(this.m_SlideTween);
		return this.m_SlideTween.tweens[0]._playing;
	}

	setPosition(newPos)
	{
		this.m_Sprite.position.set(newPos.x, newPos.y);
	}

	moveTo(newPos, duration)
	{
		this.m_Text.position.set(this.m_Sprite.position.x, this.m_Sprite.position.y);
		this.m_SlideTween = PEXICharm.slide(this.m_Sprite, newPos.x, newPos.y, duration);
		this.m_SlideTween.play()
		// console.log(this.m_SlideTween.tweens[0]._playing+"-"+this.m_SlideTween.tweens[1]._playing);
	}
}
require('pixi.js');

global.gPEXI 			= require('pexi');
global.DeviceInfo		= require('./DeviceInfo');
global.GameConfig		= require('./Config');
global.APP				= require('./Application');
global.Input			= require('./Input');
global.Timer			= require('./Timer');
global.StateManager		= require('./StateManager');
global.GameDefine		= require('./game/GameDefine');

global.StatePreLoad		= require('./states/StatePreLoad');
// global.StateLoading		= require('./states/StateLoading');
global.StateInGame		= require('./states/StateInGame');
// global.StateGameOver	= require('./states/StateGameOver');

global.gTotalTimeSpent 	= 0;
global.gIsSizeChanged	= false;

let gIsInterrupt		= false;

function GameLoop(deltaTime)
{
	deltaTime = deltaTime / (60 * APP.ticker.speed);
	if (!gIsInterrupt)
	{
		if (gTotalTimeSpent > 0)
		{
			gTotalTimeSpent += deltaTime;
		}
		APP.Update(deltaTime);
		APP.Render();	
	}
}

function PageShow()
{
	StateManager.Resume();
}

function PageHide()
{
	StateManager.Pause();
}

function Pause()
{
	if (!gIsInterrupt)
	{
		gIsInterrupt = true;
		StateManager.Pause();
	}
}

function Resume()
{
	if (gIsInterrupt)
	{
		gIsInterrupt = false;
		StateManager.Resume();
	}
}

window.onBackPressed = function()
{
	StateManager.Back();
}

window.onGamePause = function()
{
	Pause();
}

window.onGameResume = function()
{
	Resume();
}

window.onresize = function()
{
	if (!gIsSizeChanged)
	{
		APP.Resize();
		StateManager.Resize();
	}
}

window.main = function()
{
	resource.load_css("../css/style.css");
    StateManager.PushState(StatePreLoad);
	
	APP.Init(GameLoop);
}
global.GameManager		= require('../game/GameManager');
global.BallManager		= require('../game/BallManager.js');
global.PEXICharm		= null;

class StateInGame extends PIXI.Container
{
	constructor()
	{
		super();
		PEXICharm = new gPEXI.Charm(PIXI);
		this.addChild(GameManager);
	}

	Init()
	{
		GameManager.Init();
	}

	Load()
	{
		this.Init();

		APP.AddChild(this);
		APP.Align(this);
	}

	Unload()
	{
		APP.RemoveChild(this);
	}

	Back()
	{

	}

	Update(deltaTime)
	{
		PEXICharm.update();
		GameManager.Update(deltaTime);
		BallManager.Update(deltaTime);
	}

	LoadingError()
	{

	}

	TouchHandler(event)
	{
		GameManager.TouchHandler(event);
	}

	// To this point the game don't use keyboard for input
	KeyHandler(event)
	{
		GameManager.KeyHandler(event)
	}
}
module.exports = new StateInGame();
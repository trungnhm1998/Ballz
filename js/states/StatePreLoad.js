class StatePreLoad extends PIXI.Container
{
    constructor()
    {
        super()

        this.LoadProgressHandler = this.LoadProgressHandler.bind(this)
        this.LoadErrorHandler = this.LoadErrorHandler.bind(this)
        this.LoadCompleteHandler = this.LoadCompleteHandler.bind(this)
        this.LoadGuiComplete = this.LoadGuiComplete.bind(this)
        // resource.load_font('MainFont', Utils.OMSGetData('font/OpenSans-Bold.ttf'))

        this.isLoaded = true;
    }

    Load()
    {
        APP.AddChild(this)
        APP.Align(this)

        PIXI.loader
        .once('error', this.LoadErrorHandler)
        .once('complete', this.LoadCompleteHandler)
        .on('progress', this.LoadProgressHandler)
        .load()
    }

    Unload()
    {
        APP.RemoveChild(this)
        document.removeEventListener('LoadingError', this.LoadErrorHandler)
    }

    Update(deltaTime)
    {
        if(this.isLoaded)
        {
            this.NextState()
            this.isLoaded = false;
        }
    }

    LoadProgressHandler(loader, resource)
    {
        console.log('StatePreload loading: ' + resource.name)
        console.log('StatePreload progress: ' + loader.progress + '%')
    }

    LoadErrorHandler(error)
    {
        console.log('LoadErrorHandler: ' + error)
    }

    LoadCompleteHandler()
    {   
    }

    LoadGuiComplete()
    {
        
    }

    NextState()
    {
        StateManager.SwitchState(StateInGame, false)
    }

    TouchHandler(event)
    {
    }
}
module.exports = new StatePreLoad()

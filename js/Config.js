global.resource = require('./debug.js')
class GameConfig
{
	constructor()
	{
		this.soundMax					= 20;
		this.scale						= 1;
		this.fontList					= {MainFont:'MainFont', TitleFont:'TitleFont', AltTitleFont:'AltTitleFont', BoldMainFont:'BoldMainFont' };
		this.fontInfo					= [this.fontList.MainFont, this.fontList.TitleFont, this.fontList.AltTitleFont, this.fontList.BoldMainFont ];
		this.isSupportTwoScreenSize		= false;
		this.isReverseScaleRatio		= false;
		this.isMigGamePortrait			= true;

		this.CalculateScreenSize();
	}

	CalculateScreenSize()
	{
		this.width	= 750;
		this.height	= 1334;
	}
}
module.exports = new GameConfig();